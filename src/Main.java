import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequencer;

public class Main {

	public static void main(String[] args) {
		try {

			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();

			System.out.print("\u001b[2J" + "\u001b[H");
			System.out.flush();
			String looped = "";
			for (int i = 0; i < args.length; i++) {
				switch (args[i]) {
				case "-loop":
					sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
					looped = "|LOOP|";
					break;
				case "-l":
					sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
					looped = "|LOOP|";
					break;
				case "-help":
					System.out.println("NAME");
					System.out.println("JUMP - a simple MIDI player.");
					System.out.println("SYNOPSIS");
					System.out.println("JUMP <ARGUMENTS> <MIDIFILE>");
					System.out.println("DESCRIPTION");
					System.out.println("JUMP is stands for \"Java Unified MIDI Player\".");
					System.out.println("This program supports MID format.");
					System.out.println("ARGUMENTS");
					System.out.println("-h, -help : this help text");
					System.out.println("-l, -loop : loop midi-file");
					System.exit(0);
					break;
				case "-h":
					System.out.println("NAME");
					System.out.println("JUMP - a simple MIDI player.");
					System.out.println("SYNOPSIS");
					System.out.println("JUMP <ARGUMENTS> <MIDIFILE>");
					System.out.println("DESCRIPTION");
					System.out.println("JUMP is stands for \"Java Unified MIDI Player\".");
					System.out.println("This program supports MID format.");
					System.out.println("ARGUMENTS");
					System.out.println("-h, -help : this help text");
					System.out.println("-l, -loop : loop midi-file");
					System.exit(0);
					break;
				default:
					break;
				}
			}
			InputStream stream = new FileInputStream(new File(args[args.length - 1]));
			sequencer.setSequence(stream);
			sequencer.start();
			stream.close();

			while (true) {
				System.out.print("\u001b[2J" + "\u001b[H");
				System.out.flush();
				System.out.println(args[args.length - 1] + " " + looped);

				System.out.print("[");
				int i = 1;
				for (; i <= (int) (sequencer.getMicrosecondPosition() / (sequencer.getMicrosecondLength() / 100f) / 100f
						* 50f-1); i++) {
					System.out.print(".");
				}
				for (; i < 50; i++) {
					System.out.print(" ");
				}
				System.out.print("] ");

				System.out.print(
						String.format("%02d:%02d", TimeUnit.MICROSECONDS.toMinutes(sequencer.getMicrosecondPosition()),
								TimeUnit.MICROSECONDS.toSeconds(sequencer.getMicrosecondPosition()
										- TimeUnit.MICROSECONDS.toSeconds(sequencer.getMicrosecondPosition()))));
				System.out.print("/");
				System.out.println(
						String.format("%02d:%02d", TimeUnit.MICROSECONDS.toMinutes(sequencer.getMicrosecondLength()),
								TimeUnit.MICROSECONDS.toSeconds(sequencer.getMicrosecondLength()
										- TimeUnit.MICROSECONDS.toSeconds(sequencer.getMicrosecondLength()))));

				System.out.println("pl(a)y, pau(s)e, s(t)op | re(w)ind, fo(r)ward, mut(e), (l)oop | e(x)it");

				switch (RawConsoleInput.read(false)) {
				case 97:
					// play
					sequencer.start();
					break;
				case 115:
					// pause
					sequencer.stop();
					break;
				case 116:
					// stop
					sequencer.stop();
					sequencer.setMicrosecondPosition(0);
					break;
				case 119:
					// rewind
					sequencer.setMicrosecondPosition(sequencer.getMicrosecondPosition() - 1000000);
					break;
				case 114:
					// forward
					sequencer.setMicrosecondPosition(sequencer.getMicrosecondPosition() + 1000000);
					break;
				case 101:
					// mute
					for (int j = 0; j < sequencer.getSequence().getTracks().length; j++) {
						sequencer.setTrackMute(j, !sequencer.getTrackMute(j));
					}
					break;
				case 108:
					// loop
					if (sequencer.getLoopCount() != Sequencer.LOOP_CONTINUOUSLY) {
						looped = "|LOOP|";
						sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
					} else if (sequencer.getLoopCount() == Sequencer.LOOP_CONTINUOUSLY) {
						looped = "";
						sequencer.setLoopCount(0);
					}
					break;
				case 120:
					// exit
					System.exit(0);
					break;
				default:
					break;
				}

				Thread.sleep(250);
			}

		} catch (Exception e) {
		}
	}
}